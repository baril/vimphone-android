package com.vimtura.vimphone.header;

import org.linphone.core.LinphoneAddress;
import org.linphone.core.LinphoneCall;
import org.linphone.core.LinphoneCallParams;
import org.linphone.mediastream.Log;

import com.vimtura.vimphone.LinphoneUtils;
import com.vimtura.vimphone.R;
import com.vimtura.vimphone.util.DialUtil;

import android.app.Activity;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * VimturaContact
 * A dynamic contact created from SIP headers.
 * 
 * @author david
 *
 */
public class VimturaContact {
	
	public final static String HEADER_CONTACT_NAME = "X-Vimtura-Contact-Name";
	public final static String HEADER_CONTACT_IMAGE_URL = "X-Vimtura-Contact-Image-Url";
	public final static String HEADER_CALLER_ID = "X-Vimtura-Caller-Id";
	
	protected VimturaHeader header;
	protected String contactName;
	protected Uri contactImageUrl;
	protected String callerId;
	
	public VimturaContact(VimturaHeader header) {
		this.header = header;
		this.parse();
	}
	
	protected void parse() {
		this.contactName = header.getHeader(HEADER_CONTACT_NAME);
		
		String contactImageUrl = header.getHeader(HEADER_CONTACT_IMAGE_URL);
		if (contactImageUrl != null) {
			try {
				this.contactImageUrl = Uri.parse(contactImageUrl);
			} catch (Throwable t) {
				Log.e("Failed to parse URL from " + HEADER_CONTACT_IMAGE_URL + " SIP header with value of '" + contactImageUrl + "'", t);
			}
		}
		
		this.callerId = header.getHeader(HEADER_CALLER_ID);
	}	
	
	public VimturaHeader getHeader() {
		return header;
	}

	public String getContactName() {
		return contactName;
	}

	public Uri getContactImageUrl() {
		return contactImageUrl;
	}

	public String getCallerId() {
		return callerId;
	}

	public void fillOutView(Activity activity, TextView numberView, TextView nameView, TextView singleView, ImageView pictureView) {
		LinphoneCall call = this.getHeader().getCall();
		LinphoneAddress address = call.getRemoteAddress();
		
		// May be greatly sped up using a drawable cache
		// DJR: No idea what the comment above means, this is spaghetti code.
		// -- Need to call this here as it sets the display name.
		Uri uri = LinphoneUtils.findUriPictureOfContactAndSetDisplayName(address, activity.getContentResolver());
		if (pictureView != null) {
			if (this.getContactImageUrl() != null) {
				LinphoneUtils.loadImage(this.getContactImageUrl(),
						pictureView, R.drawable.unknown_small, R.drawable.unknown_small);
			} else {
				LinphoneUtils.setImagePictureFromUri(activity, pictureView,
						uri, R.drawable.unknown_small);
			}
		}
		
		if (nameView != null) {
			if (this.getContactName() != null) {
				nameView.setText(this.getContactName());
			} else {
				// To be done after findUriPictureOfContactAndSetDisplayName called
				nameView.setText(DialUtil.formatDisplayNumber(address.getDisplayName()));
			}
		}
	
		if (numberView != null) {
			if (activity.getResources().getBoolean(R.bool.only_display_username_if_unknown)) {
				numberView.setText(DialUtil.formatDisplayNumber(address.getUserName()));
			} else {
				numberView.setText(address.asStringUriOnly());
			}
			
			if (numberView.getText() == null || numberView.getText().equals("")) {
				numberView.setText(activity.getString(R.string.unknown_caller_number));
			}
			
			if (nameView != null) {
				if (nameView.getText().equals(numberView.getText())) {
					nameView.setText(null);
				}
			}
		}
		
		// Defaults
		if (nameView != null) {
			if (nameView.getText() == null || nameView.getText().equals("")) {
				if (numberView != null) {
					singleView.setText(numberView.getText());
					singleView.setVisibility(View.VISIBLE);
					nameView.setVisibility(View.GONE);
					numberView.setVisibility(View.GONE);
				} else {
					nameView.setText(activity.getString(R.string.unknown_caller_number));					
				}
			}
		}
	}
	
	public String toString() {
		return (new StringBuilder())
			.append("{ VimturaContact [")
			.append(" contactName: ").append(contactName)
			.append(", contactImageUrl: ").append(contactImageUrl)	
			.append(", callerId: ").append(callerId)				
			.append("] }")
			.toString();
	}	
	
}
