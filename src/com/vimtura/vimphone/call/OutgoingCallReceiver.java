package com.vimtura.vimphone.call;

import org.linphone.core.LinphoneCore;
import org.linphone.mediastream.Log;

import com.vimtura.vimphone.LinphoneActivity;
import com.vimtura.vimphone.LinphoneManager;
import com.vimtura.vimphone.util.DialUtil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class OutgoingCallReceiver extends BroadcastReceiver  {

	@Override
	public void onReceive(Context context, Intent intent) {
		// First ensure the app is still running.
		LinphoneCore lc = null;
		try {
			lc = LinphoneManager.getLcIfManagerNotDestroyedOrNull();
		} catch (Throwable t) {
			// ignore
		}
		if (lc == null) {
			Log.i("Not intercepting outgoing call (Linphone is not running)");
			return;
		}
		
		final String number = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
		if (number != null) {
			final String finalNumber = DialUtil.handleOutgoingCall(number);
			if (finalNumber != null) {
				Log.i("Handling outgoing call to: " + finalNumber);
				
				// Setting null result data will prevent the default android dialler from dialling.
				setResultData(null);		
				
				Intent i = new Intent(context, LinphoneActivity.class);
				i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				i.putExtra(Intent.EXTRA_PHONE_NUMBER, finalNumber);
		        context.startActivity(i); 
			}
		}
	}

}
