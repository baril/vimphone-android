/*
LinphoneService.java
Copyright (C) 2010  Belledonne Communications, Grenoble, France

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/
package com.vimtura.vimphone;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.linphone.core.LinphoneAddress;
import org.linphone.core.LinphoneCall;
import org.linphone.core.LinphoneCall.State;
import org.linphone.core.LinphoneCore;
import org.linphone.core.LinphoneCore.GlobalState;
import org.linphone.core.LinphoneCore.RegistrationState;
import org.linphone.core.LinphoneCoreException;
import org.linphone.core.LinphoneCoreFactoryImpl;
import org.linphone.core.LinphoneProxyConfig;
import org.linphone.core.OnlineStatus;
import org.linphone.mediastream.Log;
import org.linphone.mediastream.Version;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.NotificationCompat;

import com.vimtura.vimphone.R;
import com.vimtura.vimphone.LinphoneManager.NewOutgoingCallUiListener;
import com.vimtura.vimphone.LinphoneSimpleListener.LinphoneServiceListener;
import com.vimtura.vimphone.compatibility.Compatibility;
import com.vimtura.vimphone.status.StatusType;

/**
 * 
 * Linphone service, reacting to Incoming calls, ...<br />
 * 
 * Roles include:<ul>
 * <li>Initializing LinphoneManager</li>
 * <li>Starting C libLinphone through LinphoneManager</li>
 * <li>Reacting to LinphoneManager state changes</li>
 * <li>Delegating GUI state change actions to GUI listener</li>
 * 
 * 
 * @author Guillaume Beraudo
 *
 */
public final class LinphoneService extends Service implements LinphoneServiceListener {
	/* Listener needs to be implemented in the Service as it calls
	 * setLatestEventInfo and startActivity() which needs a context.
	 */
	
	private final static int NOTIF_ID=1;
	private final static int INCALL_NOTIF_ID=2;
	private final static int MESSAGE_NOTIF_ID=3;
	private final static int CUSTOM_NOTIF_ID=4;
	
	private static final Class<?>[] mSetForegroundSignature = new Class[] {
	    boolean.class};
	private static final Class<?>[] mStartForegroundSignature = new Class[] {
	    int.class, Notification.class};
	private static final Class<?>[] mStopForegroundSignature = new Class[] {
	    boolean.class};

	private NotificationManager mNM;
	private Method mSetForeground;
	private Method mStartForeground;
	private Method mStopForeground;
	private Object[] mSetForegroundArgs = new Object[1];
	private Object[] mStartForegroundArgs = new Object[2];
	private Object[] mStopForegroundArgs = new Object[1];	
	
	private Class<? extends Activity> incomingReceivedActivity = LinphoneActivity.class;	
	
	/** TODO REMOVE
	private Notification mIncallNotif;
	private Notification mMsgNotif;
	private Notification mCustomNotif;
	
	private PendingIntent mNotifContentIntent;
	**/
	
	private Integer mMsgNotifCount = null;	
	private String mNotificationTitle;

	private Handler mHandler = new Handler();
	private static LinphoneService instance;

//	private boolean mTestDelayElapsed; // add a timer for testing
	private boolean mTestDelayElapsed = true; // no timer
	private WifiManager mWifiManager ;
	private WifiLock mWifiLock;
	
	private enum IncallIconState {INCALL, PAUSE, VIDEO, IDLE}
	private IncallIconState mCurrentIncallIconState = IncallIconState.IDLE;	
	
	public static boolean isReady() {
		return instance!=null && instance.mTestDelayElapsed;
	}

	/**
	 * @throws RuntimeException service not instantiated
	 */
	public static LinphoneService instance()  {
		if (isReady()) return instance;

		throw new RuntimeException("LinphoneService not instantiated yet");
	}
	
	public void resetMessageNotifCount() {
		mMsgNotifCount = 0;
	}

	@Override
	public void onCreate() {
		super.onCreate();

		// In case restart after a crash. Main in LinphoneActivity
		LinphonePreferenceManager.getInstance(this);

		// Set default preferences
		PreferenceManager.setDefaultValues(this, R.xml.preferences, true);

		// Initilize the image loader (and cache)
		LinphoneUtils.initilizeImageLoader(this);
		
		// Prepare member properties
		mNotificationTitle = getString(R.string.notification_title);

		// Dump some debugging information to the logs
		Log.i(START_LINPHONE_LOGS);
		dumpDeviceInformation();
		dumpInstalledLinphoneInformation();

		mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		mNM.cancel(INCALL_NOTIF_ID); // in case of crash the icon is not removed

		LinphoneManager.createAndStart(this, this);
		mWifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		mWifiLock = mWifiManager.createWifiLock(WifiManager.WIFI_MODE_FULL_HIGH_PERF, this.getPackageName()+"-wifi-call-lock");
		mWifiLock.setReferenceCounted(false);
		instance = this; // instance is ready once linphone manager has been created
		
		initServiceForegroundMethods();
	    
	    // Create initial notification
	    Notification initialNotification = buildNotification(StatusType.ORANGE, getString(R.string.notification_started));
		startForegroundCompat(NOTIF_ID, initialNotification);

		if (!mTestDelayElapsed) {
			// Only used when testing. Simulates a 5 seconds delay for launching service
			mHandler.postDelayed(new Runnable() {
				@Override public void run() {
					mTestDelayElapsed = true;
				}
			}, 5000);
		}
		
		LinphoneManager.getLc().setPresenceInfo(0, "", OnlineStatus.Online);
	}

	void initServiceForegroundMethods() {
		// Retrieve methods to publish notification and keep Android
		// from killing us and keep the audio quality high.
	    try {
	        mStartForeground = getClass().getMethod("startForeground",
	                mStartForegroundSignature);
	        mStopForeground = getClass().getMethod("stopForeground",
	                mStopForegroundSignature);
	        return;
	    } catch (NoSuchMethodException e) {
	        // Running on an older platform.
	        mStartForeground = mStopForeground = null;
	    }
	    try {
	        mSetForeground = getClass().getMethod("setForeground",
	                mSetForegroundSignature);
	    } catch (NoSuchMethodException e) {
	        throw new IllegalStateException(
	                "OS doesn't have Service.startForeground OR Service.setForeground!");
	    }		
	}
	
	void invokeMethod(Method method, Object[] args) {
	    try {
	        method.invoke(this, args);
	    } catch (InvocationTargetException e) {
	        // Should not happen.
	        Log.w("LinphoneService", "Unable to invoke method", e);
	    } catch (IllegalAccessException e) {
	        // Should not happen.
	        Log.w("LinphoneService", "Unable to invoke method", e);
	    }
	}

	/**
	 * This is a wrapper around the new startForeground method, using the older
	 * APIs if it is not available.
	 */
	void startForegroundCompat(int id, Notification notification) {
	    // If we have the new startForeground API, then use it.
	    if (mStartForeground != null) {
	        mStartForegroundArgs[0] = Integer.valueOf(id);
	        mStartForegroundArgs[1] = notification;
	        invokeMethod(mStartForeground, mStartForegroundArgs);
	        return;
	    }

	    // Fall back on the old API.
	    mSetForegroundArgs[0] = Boolean.TRUE;
	    invokeMethod(mSetForeground, mSetForegroundArgs);    
	    notifyWrapper(id, notification);
	}

	/**
	 * This is a wrapper around the new stopForeground method, using the older
	 * APIs if it is not available.
	 */
	void stopForegroundCompat(int id) {
	    // If we have the new stopForeground API, then use it.
	    if (mStopForeground != null) {
	        mStopForegroundArgs[0] = Boolean.TRUE;
	        invokeMethod(mStopForeground, mStopForegroundArgs);
	        return;
	    }

	    // Fall back on the old API.  Note to cancel BEFORE changing the
	    // foreground state, since we could be killed at that point.
	    mNM.cancel(id);
	    mSetForegroundArgs[0] = Boolean.FALSE;
	    invokeMethod(mSetForeground, mSetForegroundArgs);
	}	
	
	private synchronized void setIncallIcon(IncallIconState state) {
		if (state == mCurrentIncallIconState) return;
		mCurrentIncallIconState = state;

		int notificationTextId = 0;
		int inconId = 0;
		
		switch (state) {
		case IDLE:
			mNM.cancel(INCALL_NOTIF_ID);
			return;
		case INCALL:
			inconId = R.drawable.conf_unhook;
			notificationTextId = R.string.incall_notif_active;
			break;
		case PAUSE:
			inconId = R.drawable.conf_status_paused;
			notificationTextId = R.string.incall_notif_paused;
			break;
		case VIDEO:
			inconId = R.drawable.conf_video;
			notificationTextId = R.string.incall_notif_video;
			break;	
		default:
			throw new IllegalArgumentException("Unknown state " + state);
		}
		
		if (LinphoneManager.getLc().getCallsNb() == 0) {
			mNM.cancel(INCALL_NOTIF_ID);
			return;
		}
		
		LinphoneCall call = LinphoneManager.getLc().getCalls()[0];
		String userName = call.getRemoteAddress().getUserName();
		String domain = call.getRemoteAddress().getDomain();
		String displayName = call.getRemoteAddress().getDisplayName();
		LinphoneAddress address = LinphoneCoreFactoryImpl.instance().createLinphoneAddress("sip:" + userName + "@" + domain);
		address.setDisplayName(displayName);

		Uri pictureUri = LinphoneUtils.findUriPictureOfContactAndSetDisplayName(address, getContentResolver());
		Bitmap bm = null;
		try {
			bm = MediaStore.Images.Media.getBitmap(getContentResolver(), pictureUri);
		} catch (Exception e) {
			bm = BitmapFactory.decodeResource(getResources(), R.drawable.unknown_small);
		}
		String name = address.getDisplayName() == null ? address.getUserName() : address.getDisplayName();

		Intent notifIntent = new Intent(this, LinphoneActivity.class);
		notifIntent.putExtra("Notification", true);
		
		PendingIntent notifContentIntent = PendingIntent.getActivity(this, 0, notifIntent, PendingIntent.FLAG_ONE_SHOT);		
		
		// Assemble notification
		NotificationCompat.Builder notifBuilder = assembleBasicNotificationBuilder(inconId, getString(notificationTextId), null, name, notifContentIntent);
		if (bm != null) {
			notifBuilder.setLargeIcon(bm);
		}
		
		notifyWrapper(INCALL_NOTIF_ID, notifBuilder.build());
	}

	public void refreshIncallIcon(LinphoneCall currentCall) {
		LinphoneCore lc = LinphoneManager.getLc();
		if (currentCall != null) {
			if (currentCall.getCurrentParamsCopy().getVideoEnabled() && currentCall.cameraEnabled()) {
				// checking first current params is mandatory
				setIncallIcon(IncallIconState.VIDEO);
			} else {
				setIncallIcon(IncallIconState.INCALL);
			}
		} else if (lc.getCallsNb() == 0) {
			setIncallIcon(IncallIconState.IDLE);
		}  else if (lc.isInConference()) {
			setIncallIcon(IncallIconState.INCALL);
		} else {
			setIncallIcon(IncallIconState.PAUSE);
		}
	}

	public void addNotification(Intent onClickIntent, int iconResourceID, String title, String message) {
		PendingIntent notifContentIntent = PendingIntent.getActivity(this, 0, onClickIntent, PendingIntent.FLAG_ONE_SHOT);
		
		NotificationCompat.Builder notifBuilder = assembleBasicNotificationBuilder(iconResourceID, message, null, title, notifContentIntent);
		
		notifyWrapper(CUSTOM_NOTIF_ID, notifBuilder.build());		
	}
	
	public void removeCustomNotification() {
		mNM.cancel(CUSTOM_NOTIF_ID);
	}
	
	public void displayMessageNotification(String fromSipUri, String fromName, String message) {
		Intent notifIntent = new Intent(this, LinphoneActivity.class);
		notifIntent.putExtra("GoToChat", true);
		notifIntent.putExtra("ChatContactSipUri", fromSipUri);
		
		PendingIntent notifContentIntent = PendingIntent.getActivity(this, 0, notifIntent, PendingIntent.FLAG_ONE_SHOT);
		
		if (fromName == null) {
			fromName = fromSipUri;
		}
		
		if (mMsgNotifCount == null) {
			mMsgNotifCount = 1;
		} else {
			mMsgNotifCount++;
		}
		
		Uri pictureUri = LinphoneUtils.findUriPictureOfContactAndSetDisplayName(LinphoneCoreFactoryImpl.instance().createLinphoneAddress(fromSipUri), getContentResolver());
		Bitmap bm = null;
		try {
			bm = MediaStore.Images.Media.getBitmap(getContentResolver(), pictureUri);
		} catch (Exception e) {
			bm = BitmapFactory.decodeResource(getResources(), R.drawable.unknown_small);
		}	
		
		String title;
		if (mMsgNotifCount == 1) {
			title = "Unread message from " + fromName;
		} else {
			title = mMsgNotifCount + " unread messages";
		}		
		
		NotificationCompat.Builder notifBuilder = assembleBasicNotificationBuilder(R.drawable.chat_icon_over, message, null, title, notifContentIntent);
		if (bm != null) {
			notifBuilder.setLargeIcon(bm);
		}
		
		notifyWrapper(MESSAGE_NOTIF_ID, notifBuilder.build());			
	}
	
	public void removeMessageNotification() {
		mNM.cancel(MESSAGE_NOTIF_ID);
	}

	public static final String START_LINPHONE_LOGS = " ==== Phone information dump ====";
	private void dumpDeviceInformation() {
		StringBuilder sb = new StringBuilder();
		sb.append("DEVICE=").append(Build.DEVICE).append("\n");
		sb.append("MODEL=").append(Build.MODEL).append("\n");
		//MANUFACTURER doesn't exist in android 1.5.
		//sb.append("MANUFACTURER=").append(Build.MANUFACTURER).append("\n");
		sb.append("SDK=").append(Build.VERSION.SDK_INT);
		Log.i(sb.toString());
	}

	private void dumpInstalledLinphoneInformation() {
		PackageInfo info = null;
		try {
		    info = getPackageManager().getPackageInfo(getPackageName(),0);
		} catch (NameNotFoundException nnfe) {}

		if (info != null) {
			Log.i("Linphone version is ", info.versionName + " (" + info.versionCode + ")");
		} else {
			Log.i("Linphone version is unknown");
		}
	}
	
	private Notification buildNotification(StatusType statusType, String contentText) {
		return this.buildNotification(statusType, contentText, null);
	}	
	
	private Notification buildNotification(StatusType statusType, String contentText, String tickerText) {
		return this.assembleNotificationBuilder(statusType, contentText, tickerText).build();
	}

	private NotificationCompat.Builder assembleNotificationBuilder(StatusType statusType, String contentText) {
		return this.assembleNotificationBuilder(statusType, contentText, null);
	}
	
	private NotificationCompat.Builder assembleNotificationBuilder(StatusType statusType, String contentText, String tickerText) {
		NotificationCompat.Builder builder = assembleBasicNotificationBuilder(statusType.getSmallIcon(), contentText, tickerText);
		
		Resources res = this.getResources();

		// Optional fields
		if (statusType.getLargeIcon() != null) {
			builder.setLargeIcon(BitmapFactory.decodeResource(res, statusType.getLargeIcon()));
		}
		if (statusType.isLedEnabled()) {
			builder.setLights(statusType.getLightRgb(), statusType.getLightOnMs(), statusType.getLightOffMs());
		}
		
		return builder;		
	}
	
	private NotificationCompat.Builder assembleBasicNotificationBuilder(int iconId, String contentText, String tickerText) {
		return this.assembleBasicNotificationBuilder(iconId, contentText, tickerText, null);
	}	
	
	private NotificationCompat.Builder assembleBasicNotificationBuilder(int iconId, String contentText, String tickerText, String contentTitle) {
		return this.assembleBasicNotificationBuilder(iconId, contentText, tickerText, contentTitle, null);
	}
	
	private NotificationCompat.Builder assembleBasicNotificationBuilder(int iconId, String contentText, String tickerText, String contentTitle, PendingIntent contentIntent) {
		if (tickerText == null) {
			tickerText = contentText;
		}
		if (contentTitle == null) {
			contentTitle = mNotificationTitle;
		}
		
		if (contentIntent == null) {
			Intent notificationIntent = new Intent(this, incomingReceivedActivity);
			contentIntent = PendingIntent.getActivity(this,
			        0, notificationIntent,
			        PendingIntent.FLAG_UPDATE_CURRENT);
		}

		Resources res = this.getResources();

		NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
			.setContentIntent(contentIntent)
		    .setSmallIcon(iconId)
		    .setTicker(tickerText)
		    .setWhen(System.currentTimeMillis())
		    .setAutoCancel(true)
		    .setContentTitle(contentTitle)
		    .setOngoing(true)
		    .setContentText(contentText);
		
		return builder;		
	}	
	
	private synchronized void sendNotification(StatusType statusType, int textId) {
		String text = getString(textId);
		if (text.contains("%s") && LinphoneManager.getLc() != null) {
			// Test for null lc is to avoid a NPE when Android mess up badly with the String resources.
			LinphoneProxyConfig lpc = LinphoneManager.getLc().getDefaultProxyConfig();
			String id = lpc != null ? lpc.getIdentity() : "";
			text = String.format(text, id);
		}
		
		notifyWrapper(NOTIF_ID, buildNotification(statusType, text));
	}

	/**
	 * Wrap notifier to avoid setting the linphone icons while the service
	 * is stopping. When the (rare) bug is triggered, the linphone icon is
	 * present despite the service is not running. To trigger it one could
	 * stop linphone as soon as it is started. Transport configured with TLS.
	 */
	private synchronized void notifyWrapper(int id, Notification notification) {
		if (instance != null) {
			mNM.notify(id, notification);
		} else {
			Log.i("Service not ready, discarding notification");
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public synchronized void onDestroy() {
		LinphoneManager.getLc().setPresenceInfo(0, "", OnlineStatus.Offline);
		instance = null;
		LinphoneManager.destroy();

	    // Make sure our notification is gone.
	    stopForegroundCompat(NOTIF_ID);
	    mNM.cancel(INCALL_NOTIF_ID);
	    mNM.cancel(MESSAGE_NOTIF_ID);
	    mWifiLock.release();
	    
		super.onDestroy();
	}
	
	private static final LinphoneGuiListener guiListener() {
		return null;
	}	

	public void onDisplayStatus(final String message) {
		mHandler.post(new Runnable() {
			public void run() {
				if (guiListener() != null) guiListener().onDisplayStatus(message);				
			}
		});
	}

	public void onGlobalStateChanged(final GlobalState state, final String message) {
		if (state == GlobalState.GlobalOn) {
			sendNotification(StatusType.OFFLINE, R.string.notification_started);

			// Slightly delay the propagation of the state change.
			// This is to let the linphonecore finish to be created
			// in the java part.
			mHandler.postDelayed(new Runnable() {
				public void run() {
					if (guiListener() != null)
						guiListener().onGlobalStateChangedToOn(message);				
				}
			}, 50);
		}
	}

	public void onRegistrationStateChanged(final RegistrationState state,
			final String message) {
		
		if (state == RegistrationState.RegistrationOk && LinphoneManager.getLc().getDefaultProxyConfig() != null && LinphoneManager.getLc().getDefaultProxyConfig().isRegistered()) {
			sendNotification(StatusType.GREEN, R.string.notification_registered);
		} else if ((state == RegistrationState.RegistrationFailed || state == RegistrationState.RegistrationCleared) && (LinphoneManager.getLc().getDefaultProxyConfig() == null || !LinphoneManager.getLc().getDefaultProxyConfig().isRegistered())) {
			sendNotification(StatusType.RED, R.string.notification_register_failure);
		} else if (state == RegistrationState.RegistrationNone) {
			sendNotification(StatusType.OFFLINE, R.string.notification_offline);
		}

		mHandler.post(new Runnable() {
			public void run() {
				if (LinphoneActivity.isInstanciated()) {
					LinphoneActivity.instance().onRegistrationStateChanged(state);
				}
			}
		});
	}
	
	public void setActivityToLaunchOnIncomingReceived(Class<? extends Activity> activity) {
		incomingReceivedActivity = activity;
	}
	
	protected void onIncomingReceived() {
		//wakeup linphone
		startActivity(new Intent()
				.setClass(this, incomingReceivedActivity)
				.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
	}

	public void onCallStateChanged(final LinphoneCall call, final State state, final String message) {
		if (instance == null) {
			Log.i("Service not ready, discarding call state change to ",state.toString());
			return;
		}
		
		if (state == LinphoneCall.State.IncomingReceived) {
			onIncomingReceived();
		}
		
		if (state == State.CallUpdatedByRemote) {
			// If the correspondent proposes video while audio call
			boolean remoteVideo = call.getRemoteParams().getVideoEnabled();
			boolean localVideo = call.getCurrentParamsCopy().getVideoEnabled();
			boolean autoAcceptCameraPolicy = LinphoneManager.getInstance().isAutoAcceptCamera();
			if (remoteVideo && !localVideo && !autoAcceptCameraPolicy && !LinphoneManager.getLc().isInConference()) {
				try {
					LinphoneManager.getLc().deferCallUpdate(call);
				} catch (LinphoneCoreException e) {
					e.printStackTrace();
				}
			}
		}

		if (state == State.StreamsRunning) {
			// Workaround bug current call seems to be updated after state changed to streams running
			if (getResources().getBoolean(R.bool.enable_call_notification))
				refreshIncallIcon(call);
			mWifiLock.acquire();
		} else {
			if (getResources().getBoolean(R.bool.enable_call_notification))
				refreshIncallIcon(LinphoneManager.getLc().getCurrentCall());
		}
		if ((state == State.CallEnd || state == State.Error) && LinphoneManager.getLc().getCallsNb() < 1) {
			mWifiLock.release();
		}
		
		mHandler.post(new Runnable() {
			public void run() {
				if (guiListener() != null)
					guiListener().onCallStateChanged(call, state, message);
			}
		});
	}

	public void changeRingtone(String ringtone) {
		SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
		editor.putString(getString(R.string.pref_audio_ringtone), ringtone);
		editor.commit();
	}

	public void onRingerPlayerCreated(MediaPlayer mRingerPlayer) {
		String uriString = PreferenceManager.getDefaultSharedPreferences(this).getString(getString(R.string.pref_audio_ringtone), 
				android.provider.Settings.System.DEFAULT_RINGTONE_URI.toString());
		try {
			if (uriString.startsWith("content://")) {
				mRingerPlayer.setDataSource(this, Uri.parse(uriString));
			} else {
				FileInputStream fis = new FileInputStream(uriString);
				mRingerPlayer.setDataSource(fis.getFD());
				fis.close();
			}
		} catch (IOException e) {
			Log.e(e, "Cannot set ringtone");
		}
	}

	public void tryingNewOutgoingCallButAlreadyInCall() {
		mHandler.post(new Runnable() {
			public void run() {
				if (guiListener() != null)
					guiListener().onAlreadyInCall();			
			}
		});
	}

	public void tryingNewOutgoingCallButCannotGetCallParameters() {
		mHandler.post(new Runnable() {
			public void run() {
				if (guiListener() != null)
					guiListener().onCannotGetCallParameters();			
			}
		});
	}

	public void tryingNewOutgoingCallButWrongDestinationAddress() {
		mHandler.post(new Runnable() {
			public void run() {
				if (guiListener() != null)
					guiListener().onWrongDestinationAddress();			
			}
		});
	}

	public void onCallEncryptionChanged(final LinphoneCall call, final boolean encrypted,
			final String authenticationToken) {
		// IncallActivity registers itself to this event and handle it.
	}

	// Inline classes and interfaces
	public interface LinphoneGuiListener extends NewOutgoingCallUiListener {
		void onDisplayStatus(String message);
		void onGlobalStateChangedToOn(String message);
		void onCallStateChanged(LinphoneCall call, State state, String message);
	}
	
}

