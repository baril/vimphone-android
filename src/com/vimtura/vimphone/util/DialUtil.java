package com.vimtura.vimphone.util;

import org.linphone.mediastream.Log;

import com.vimtura.vimphone.LinphoneManager;
import com.vimtura.vimphone.setup.DialCountry;

public class DialUtil {
	public static DialCountry getDialCountry() {
		return LinphoneManager.getInstance().getPM().getDialCountry();
	}
	
	public static String formatDisplayNumber(String number) {
		if (number != null && !number.equals("")) {
			number = number.trim();
			
			DialCountry dc = getDialCountry();
			if (DialCountry.AU.equals(dc)) {
				if (number.startsWith("61") && number.length() == 11) {
					return "(0" + number.substring(2, 3) + ") " + number.substring(3, 7) + " " + number.substring(7);
				} else if (number.startsWith("0") && number.length() == 10) {
					return "(0" + number.substring(1, 2) + ") " + number.substring(2, 6) + " " + number.substring(6);
				}
			}
		}
		
		return number;
	}
	
	public static String formatPhoneDialNumber(String number) {
		if (number != null && !number.equals("")) {
			number = number.trim();
			
			DialCountry dc = getDialCountry();
			if (DialCountry.AU.equals(dc)) {
				if (number.startsWith("61") && number.length() == 11) {
					return "0" + number.substring(2, 3) + number.substring(3);
				}
			}
		}
		
		return number;		
	}
	
	/**
	 * Handle the interception of an outgoing call.
	 * @param destination
	 * @return null if the destination is not handled, or a destination to dial if it is handled.
	 */
	public static String handleOutgoingCall(String destination) {
		if (destination != null) {
			destination = destination.trim();
			
			LinphoneManager linphoneManager = LinphoneManager.getInstance();
			if (linphoneManager == null) {
				return null;
			}
			
			String interceptionPrefix = linphoneManager.getPM().getInterceptionPrefix();
			if (interceptionPrefix != null) {
				if (destination.startsWith(interceptionPrefix)) {
						return destination.substring(interceptionPrefix.length());
				}
			}
			
			String interceptionPattern = linphoneManager.getPM().getInterceptionPattern();
			if (interceptionPattern != null) {
				if (matchesDialingPattern(destination, interceptionPattern)) {
					return destination;
				}
			}
		}
		
		return null;
	}

	/**
	 * Attempts to detect a pattern type and match a given destination
	 * @param destination
	 * @param pattern
	 * @return true if the pattern matches, false if it doesn't match
	 */
	public static boolean matchesDialingPattern(String destination, String pattern) {
		// Replace asterisk patterns
		pattern = pattern.replace("X", "[0-9]");
		pattern = pattern.replace("Z", "[1-9]");
		pattern = pattern.replace("N", "[2-9]");
		
		if (!pattern.startsWith("^")) {
			pattern = "^" + pattern;
		}
		if (!pattern.startsWith("$")) {
			pattern += "$";
		}
		
		Log.w("Pattern = " + pattern);
		
		return destination.matches(pattern);
	}
}
